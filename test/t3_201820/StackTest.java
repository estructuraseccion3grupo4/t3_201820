package t3_201820;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;

import org.junit.jupiter.api.Test;

import model.data_structures.Stack;

class StackTest {
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	private Stack<String> list;
	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------
	void scenario0()
	{
		list= new Stack();
	}
	void scenario1()
	{
		list= new Stack();
		list.push("String0");
		list.push("String1");
		list.push("String2");
	}
	/**
	 * Cases:
	 * 1. La lista Stack esta vacia.
	 * 2. La lista Stack contiene datos.
	 */

	@Test
	void testIsEmpty() {
		scenario0();
		assertTrue(list.isEmpty(),"Stack debe estar vacio.");
		list.push("String0");
		assertFalse(list.isEmpty(), "Stack no debe estar vacio");
	}
	/**
	 * Cases:
	 * 1. La lista Stack esta vacia.
	 * 2. La lista Stack contiene datos.
	 */
	@Test
	void testSize() {
		scenario0();
		assertEquals(0, list.size(),"El tama�o es incorrecto");
		scenario1();
		assertEquals(3, list.size(),"El tama�o es incorrecto");
	}
	/**
	 * Cases:
	 * 1. Se agregaron los 3 objetos
	 * 2. La lista Stack contiene datos.
	 */
	@Test
	void testPush() {
		scenario1();
		assertEquals(3, list.size(), "No se agrego correctamente");

		try
		{
			for(int i = 0; i < list.size(); i++) {
				String actual = list.pop();
				String expected;
				
				switch(i) {
				
					case 0:
						expected = "String2";
						break;
						
					case 1:
						expected = "String1";
						break;
						
					default:
						expected = "String0";
						break;
				}
				
				if(!expected.equals(actual)) {
					fail("No se agregaron en el orden correcto");
				}
			}
		}
		catch (Exception e)
		{
			fail("No deber�a generar excepciones");

		}
	}
	/**
	 * Cases:
	 * 1. La lista Stack esta vacia.
	 * 2. La lista S elementos.
	 */
	@Test
	void testPop() 
	{
		scenario0();
		assertNull(list.pop(),"Deberia retornar null");
		 
		scenario1();
		assertEquals(list.pop(), "String2", "No se elimino el �ltimo que entro");
	}

}
