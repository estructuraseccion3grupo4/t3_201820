package t3_201820;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.data_structures.Queue;

class QueueTest {

	//-----------------------------------------------------------
	// Attributes
	//-----------------------------------------------------------

	private Queue<String> list;

	//-----------------------------------------------------------
	// Scenarios
	//-----------------------------------------------------------

	void setupScenario0() {
		list = new Queue<>();
	}

	void setupScenario1() {
		list = new Queue<>();

		list.enqueue("String1");
		list.enqueue("String2");
		list.enqueue("String3");
	}

	//-----------------------------------------------------------
	// Methods
	//-----------------------------------------------------------

	/**
	 * Casos:
	 * 	1. Queue está vacía.
	 * 	2. Queue contiene datos.
	 */
	@Test
	void testIsEmpty() {
		setupScenario0();

		assertTrue(list.isEmpty(), "El Queue debería estar vacío");

		list.enqueue("String1");

		assertFalse(list.isEmpty(), "El Queue no debería estar vacío");
	}

	/**
	 * Casos:
	 * 	1. Queue está vacío.
	 * 	2. Queue contiene datos.
	 */
	@Test
	void testSize() {
		setupScenario0();

		assertEquals(0, list.size(), "El tamaño no es correcto");

		setupScenario1();

		assertEquals(3, list.size(), "El tamaño no es correcto");
	}

	/**
	 * Casos:
	 * 	1. Se agregaron 3 elementos.
	 */
	@Test
	void testEnqueue() {
		setupScenario1();

		assertEquals(3, list.size(), "El tamaño no es correcto");

		try {
			String actual;
			String expected;
			
			for(int i = 0; i < list.size(); i++) {
				actual = list.dequeue();
				
				switch(i) {
				
					case 0:
						expected = "String1";
						break;
						
					case 1:
						expected = "String2";
						break;
						
					default:
						expected = "String3";
						break;
				}
				
				if (!expected.equals(actual)) {
					fail("No se agregaron en el orden correcto");
				}
			}
			
		} catch (Exception e) {
			fail("No debería generar excepciones");
		}

	}

	/**
	 * Casos:
	 * 	1. El Queue está vacío.
	 * 	2. El Queue contiene datos.
	 */
	@Test
	void testDequeue() {
		setupScenario0();
		assertNull(list.dequeue(), "Debería retornar null");
		
		setupScenario1();
		assertEquals(list.dequeue(), "String1", "No se sacó al primero que entró");
		assertEquals(list.dequeue(), "String2", "No se sacó el siguiente elemento");
	}

}

