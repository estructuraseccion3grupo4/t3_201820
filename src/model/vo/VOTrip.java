package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Comparable<VOTrip>{
	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	private int id;

	private String startTime;

	private String endTime;

	private int bikeId;

	private int tripDuration;

	private int fromStationId;

	private String fromStationName;

	private int toStationId;

	private String toStationName;

	private String userType;

	private String gender;

	private int birthYear;

	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------

	/**
	 * @param id
	 * @param startTime
	 * @param endTime
	 * @param bikeId
	 * @param tripDuration
	 * @param fromStationId
	 * @param fromStationName
	 * @param toStationId
	 * @param toStationName
	 * @param userType
	 * @param gender
	 * @param birthYear
	 */
	public VOTrip(int id, String startTime, String endTime, int bikeId, int tripDuration, int fromStationId,
			String fromStationName, int toStationId, String toStationName, String userType, String gender,
			int birthYear) {
		this.id = id;
		this.startTime = startTime;
		this.endTime = endTime;
		this.bikeId = bikeId;
		this.tripDuration = tripDuration;
		this.fromStationId = fromStationId;
		this.fromStationName = fromStationName;
		this.toStationId = toStationId;
		this.toStationName = toStationName;
		this.userType = userType;
		this.gender = gender;
		this.birthYear = birthYear;
	}

	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @return the bikeId
	 */
	public int getBikeId() {
		return bikeId;
	}

	/**
	 * @return the tripDuration
	 */
	public int getTripDuration() {
		return tripDuration;
	}

	/**
	 * @return the fromStationId
	 */
	public int getFromStationId() {
		return fromStationId;
	}

	/**
	 * @return the fromStationName
	 */
	public String getFromStationName() {
		return fromStationName;
	}

	/**
	 * @return the toStationId
	 */
	public int getToStationId() {
		return toStationId;
	}

	/**
	 * @return the toStationName
	 */
	public String getToStationName() {
		return toStationName;
	}

	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @return the birthYear
	 */
	public int getBirthYear() {
		return birthYear;
	}

	@Override
	public int compareTo(VOTrip o) {
		if(o.getId() == id) {
			return 0;
		} else if (o.getId() > id) {
			return 1;
		} else {
			return -1;
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		VOTrip trip = (VOTrip) obj;

		return this.compareTo(trip) == 0;
	}
}
