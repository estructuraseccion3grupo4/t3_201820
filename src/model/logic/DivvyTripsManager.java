package model.logic;

import java.io.FileReader;
import java.util.Iterator;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOBike;
import model.vo.VOTrip;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.IQueue;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager implements IDivvyTripsManager {

	private Queue<VOTrip> tripsQueue = new Queue<>();
	
	private Stack<VOTrip> tripsStack = new Stack<>();
	
	private Queue<VOBike> stationsQueue = new Queue<>();
	
	private Stack<VOBike> stationsStack = new Stack<>();
	
	public void loadStations (String stationsFile) {
		try {
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
			String[] nextLine;
			reader.readNext();
			
			while((nextLine = reader.readNext()) != null) {
				VOBike station = new VOBike(Integer.parseInt(nextLine[0]), 
						nextLine[1], nextLine[2], Double.parseDouble(nextLine[3]), 
						Double.parseDouble(nextLine[4]), Integer.parseInt(nextLine[5]), nextLine[6]);

				stationsQueue.enqueue(station);
				stationsStack.push(station);
			}
			
			reader.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void loadTrips (String tripsFile) {
		try {
			CSVReader reader = new CSVReader(new FileReader(tripsFile));
			String[] nextLine;
			reader.readNext();
			
			while((nextLine = reader.readNext()) != null) {
				VOTrip trip = null;
				
				if(nextLine[10].isEmpty() && nextLine[11].isEmpty()) {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2], 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], "", 0);
				
				} else if (nextLine[11].isEmpty()) {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2], 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], nextLine[10], 0);
					
				} else {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2], 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], nextLine[10], Integer.parseInt(nextLine[11]));
				}

				tripsQueue.enqueue(trip);
				tripsStack.push(trip);
			}
			
			reader.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Override
	public IQueue<String> getLastNStations (int bicycleId, int n) {
		Queue<String> lastStations = new Queue<>();
		Iterator<VOTrip> iter = tripsStack.iterator();
		int contador = 0;
		
		while(iter.hasNext() && contador < n) {
			VOTrip trip = iter.next();
			
			if (trip.getBikeId() == bicycleId) {
				System.out.println(trip.getId());
				lastStations.enqueue(trip.getToStationName());
				contador ++;
			}
		}
		
		return lastStations;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) 
	{
		Iterator<VOTrip> iter= tripsQueue.iterator();
		int contador=0;
		
		while(iter.hasNext() && contador < n)
		{
			VOTrip temp= iter.next();
			
			if(temp.getToStationId() == stationID) {
				contador ++;
				
				if(contador == n) {
					return temp;
				}
			}
		}
		
		return null;
	}	


}
